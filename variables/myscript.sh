#!/bin/bash

echo Hello World!

# $0 - The name of the Bash script
echo Bash name script - $0

# $# - How many arguments were passed to the Bash script.
echo Number of arguments - $#

# $@ - All the arguments supplied to the Bash script.
echo All the arguments supplied to the Bash script $@

# $$ - The process ID of the current script.
echo The process ID - $$.

# $USER - The username of the user running the script.
echo The username - $USER

# $HOSTNAME - The hostname of the machine the script is running on.
echo The hostname - $HOSTNAME

# $SECONDS - The number of seconds since the script was started.
echo The number of seconds since the script was started - $SECONDS

# $RANDOM - Returns a different random number each time is it referred to.
echo Some random number - $RANDOM

# $LINENO - Returns the current line number in the Bash script.
echo Line nr - $LINENO

# You can list all system variables by write "env" in CLI
