#!/bin/bash

# Set a variable
MYVAR=Hello
ANOTHERVAR=Fred

# Read variable
echo "$MYVAR $ANOTHERVAR"
echo

# Set
SAMPLEDIR=/etc

# Read
ls $SAMPLEDIR

VAR2='Hello Word'
echo "$VAR2"

VAR3="More $VAR2"
VAR4='More $VAR2'
echo "$VAR3"
echo "$VAR4"

MYVAR=$(ls /etc | wc -l)
echo "There are $MYVAR entries in the directory /etc"

echo Directory contain: $(ls)
