#!/bin/bash

# demonstrate variable scope 1.
var1=var1-export-1
var2=var2-export-1

# Let's verify their current value
echo "$0 :: var1 : $var1, var2 : $var2"
export var1

./export-2.sh

# Let's see what they are now
echo $0 :: var1 : $var1, var2 : $var2
